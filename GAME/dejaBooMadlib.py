def madlib():
        noun1 = input("Noun #1: ");
        noun2 = input("Noun #2: ");
        noun3 = input("Noun #3: ");
        noun_plural_1 = input("Noun (plural) #1: ");
        noun_plural_2 = input("Noun (plural) #2: ");
        body_part = input("Body Part: ");

        verb = input("Verb: ");

        adj1 = input("Adjective #1: ");
        adj2 = input("Adjective #2: ");
        adj3 = input("Adjective #3 ");
        adj4 = input("Adjective #4: ");
        adj5 = input("Adjective #5: ");
    
   

        madlib = (f"I look over at ({noun1}) (A-ha) \
                    I get {adj1} to you, you push me away (1 2 3 yeah) \
                    I cover those {body_part} of (yours) (again) \
                    I get {noun2} (you) \
                    {verb} me away again (Deja-Boo Singing) \
                    \
                    Stay, Oh {adj2} don’t turn back \
                    I get a familiar feelings, this is the first {adj3} \
                    Stay, Oh {adj4} let’s not act like you don’t know \
                    our {body_part} met \
                    \
                    Stay, Oh it’s a dejavu \
                    it seems that {noun_plural_1} met somewhere before \
                    I think I saw {noun3} somewhere \
                    Deja-Boo Girl \
                    Stay, Oh girl dejavu \
                    it seems that {noun_plural_2} met somewhere \
                    it seems that we’ve met {adj5}");
    
print(madlib)